<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class Theme_admin {

	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = '';

	 /**
     * Array of metaboxes/fields
     * @var array
     */
    protected $option_metabox = array();

	/**
 	 * Options page metabox id
 	 * @var string
 	 */
	//private $metabox_id = 'theme_option_metabox';

	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = 'Opciones del tema Idona';

	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = array();

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct() {
		// Set our title
		$this->title = __( 'Opciones del tema Idona', 'idn-theme' );
		
	}

	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_init', array( $this, 'option_fields' ) );
	}


	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		$option_tabs = self::option_fields();
		 foreach ($option_tabs as $index => $option_tab) {
        	register_setting( $option_tab['id'], $option_tab['id'] );
        }
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$option_tabs = self::option_fields();
		foreach ($option_tabs as $index => $option_tab) {
        	if ( $index == 0) {
				$this->options_page[] = add_menu_page( 
					$this->title, 
					$this->title, 
					'manage_options', 
					$option_tabs[0]['id'], 
					array( $this, 'admin_page_display' )
				);
				add_action( "admin_print_styles-{$this->options_page[$index]}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
				// Include CMB CSS in the head to avoid FOUT
			}else {
        		$this->options_page[] = add_submenu_page( 
					$option_tabs[0]['id'], 
					$this->title, 
					$option_tab['title'], 
					'manage_options', 
					$option_tab['id'], 
					array( $this, 'admin_page_display' ) 
				);
				add_action( "admin_print_styles-{$this->options_page[$index]}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
				
        	}
        }
	}
	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		$option_tabs = self::option_fields(); //get all option tabs
    	$tab_forms = array();     	   	
		?>
		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<!-- Options Page Nav Tabs -->           
            <h2 class="nav-tab-wrapper">
            	<?php foreach ($option_tabs as $option_tab) :
            		$tab_slug = $option_tab['id'];
            		$nav_class = 'nav-tab';
            		if ( $tab_slug == $_GET['page'] ) {
            			$nav_class .= ' nav-tab-active'; //add active class to current tab
            			$tab_forms[] = $option_tab; //add current tab to forms to be rendered
            		}
            	?>            	
            	<a class="<?php echo $nav_class; ?>" href="<?php menu_page_url( $tab_slug ); ?>"><?php esc_attr_e($option_tab['title']); ?></a>
            	<?php endforeach; ?>
            </h2>
            <!-- End of Nav Tabs -->
			<?php foreach ($tab_forms as $tab_form) : //render all tab forms (normaly just 1 form) ?>
            <div id="<?php esc_attr_e($tab_form['id']); ?>" class="group">
				<h3>Recuerde hacer click al boton guardar una vez finalizados los cambios, si cambia de pestaña sin guardar se perderán los cambios.</h3>
            	<?php cmb2_metabox_form( $tab_form, $tab_form['id'],array( 'cmb_styles' => false ) ); ?>
            </div>
            <?php endforeach; ?>
		</div>
		<?php

	}

	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	public function option_fields() {
		// Only need to initiate the array once per page-load
        if ( ! empty( $this->option_metabox ) ) {
            return $this->option_metabox;
        } 

		//place here the variable arrays for group type fields

		$seccion_1_fields = array(
			array(
				'name'    => __( 'Imagen', 'idn-theme' ),
				'id'      => 'image',
				'type'    => 'file'
			),
			array(
				'name'    => __( 'Título', 'idn-theme' ),
				'id'      => 'title',
				'type'    => 'text'
			),
			array(
				'name'    => __( 'copy', 'idn-theme' ),
				'id'      => 'copy',
				'type'    => 'wysiwyg'
			)
		); 

		//add fields for each tab in an array
		$header_fields = array(
			array(
				'name' => __( 'Website logo', 'idn-theme' ),
				'id'   => 'logo',
				'type' => 'file'
			),
			array(
				'name' => __( 'Website alternate logo', 'idn-theme' ),
				'desc' => __( 'JPG or PNG image', 'idn-theme' ),
				'id'   => 'alt_logo',
				'type' => 'file'
			),
			array(
				'name'    => __( 'Apple Touch Icon 57x57', 'idn-theme' ),
				'id'      => 'apple_icon_57',
				'type'    => 'file'
			),
			array(
				'name'    => __( 'Apple Touch Icon 72x72', 'idn-theme' ),
				'id'      => 'apple_icon_72',
				'type'    => 'file'
			),
			array(
				'name'    => __( 'Apple Touch Icon 114x114', 'idn-theme' ),
				'id'      => 'apple_icon_114',
				'type'    => 'file'
			),
			array(
				'name'    => __( 'Apple Touch Icon 144x144', 'idn-theme' ),
				'id'      => 'apple_icon_144',
				'type'    => 'file'
			),
		); 
		$redes_fields = array(
			array(
				'name'    => __( 'Facebook', 'idn-theme' ),
				'id'      => 'fb',
				'type'    => 'text_url'
			),
			array(
				'name'    => __( 'twitter', 'idn-theme' ),
				'id'      => 'tw',
				'type'    => 'text_url'
			),
			array(
				'name'    => __( 'Instagram', 'idn-theme' ),
				'id'      => 'inst',
				'type'    => 'text_url'
			),
		);
		$home_fields = array(
			array(
				'name' => __('Slider para el home',  'idn-theme'),
				'id' => 'home-slider',
				'type' => 'text'
			),
			array(
				'name' => __('Titulo sección uno',  'idn-theme'),
				'id' => 'title-section-1',
				'type' => 'text'
			),
			array(
				'name' => __('Copy sección uno',  'idn-theme'),
				'id' => 'copy-section-1',
				'type' => 'wysiwyg'
			),
			array(
				'id'          => 'bottom-seccion-1',
				'type'        => 'group',
				'name' => __( 'imagen + texto seccion 1', 'cmb2' ),
				'repeatable'  => false, // use false if you want non-repeatable group
				
				'fields' => $seccion_1_fields
			),
			array(
				'name' => __('Primer bloque verde',  'idn-theme'),
				'id' => 'green-block1',
				'type' => 'text'
			), 
		);
		$footer_fields = array(
			array(
				'name' => __('Footer info', 'idn-theme'),
				'id' => 'info-foot',
				'type' => 'wysiwyg',
			),
			array(
				'name'    => __( 'Footer copyright info', 'idn-theme' ),
				'id'      => 'copy-foot',
				'type'    => 'wysiwyg'
			),
			
		);
		$template_fields = array(
			array(
				'name'    => __( 'Bio Protocol Logo', 'idn-theme' ),
				'id'      => 'bio-protocol',
				'type'    => 'file'
			),
			array(
				'name'    => __( 'Bio Protocol Logo alternativo', 'idn-theme' ),
				'id'      => 'bio-protocol-alt',
				'type'    => 'file'
			),
			/* array(
				'id'          => 'sede_info',
				'type'        => 'group',
				'name' => __( 'Información de la oficina', 'cmb2' ),
				'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
					 'group_title'   => __( 'Sede {#}', 'metabox_dynamik' ), // since version 1.1.4, {#} gets replaced by row number
					 'add_button'    => __( 'Añadir otra sede', 'metabox_dynamik' ),
					 'remove_button' => __( 'quitar sede', 'metabox_dynamik' ),
					 'sortable'      => false, // beta

				),
				'fields' => $sede_fields
			), */
		);
		// Set our CMB2 tabs
		$this->option_metabox[] = array(
			'id'      => 'header_options', // TAB ID
			'title'      => 'Header Options',
			'hookup'  => false,
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( 'header_options'/*give key value to tab
				//IMPORTANT!: value must be same as tab id*/ ),
			),
			'show_names' => true,
			'fields' => $header_fields,//call the according field array
		) ;
		//social networks
		$this->option_metabox[] = array(
			'id'      => 'social_options', // TAB ID
			'title'   => 'Redes Sociales',
			'hookup'  => false,
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( 'social_options'/*give key value to tab //IMPORTANT!: value must be same as tab id*/ ),
			),
			'show_names' => true,
			'fields' =>  $redes_fields,//call the according field array
		);
		//home exclusive fields
		$this->option_metabox[] = array(
			'id'      => 'home_options',
			'title'   => 'Campos del home',
			'hookup'  => false,
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( 'home_options'/*give key value to tab //IMPORTANT!: value must be same as tab id*/, )
			),
			'show_names' => true,
			'fields' => $home_fields,//call the according field array
		);
		//footer fields
		$this->option_metabox[] = array(
			'id'      => 'footer_options',
			'title'   => 'Campos del footer',
			'hookup'  => false,
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( 'footer_options'/*give key value to tab //IMPORTANT!: value must be same as tab id*/, )
			),
			'show_names' => true,
			'fields' => $footer_fields,//call the according field array
		);
		//campos personalizables del tema
		$this->option_metabox[] = array(
			'id'      => 'template_fields',
			'title'   => 'Campos del tema',
			'hookup'  => false,
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( 'template_fields'/*give key value to tab //IMPORTANT!: value must be same as tab id*/, )
			),
			'show_names' => true,
			'fields' => $template_fields,//call the according field array
		);

		//insert extra tabs here

		return $this->option_metabox;
	}
	/**
     * Returns the option key for a given field id
     * @since  0.1.0
     * @return array
     */
    public function get_option_key($field_id) {
    	$option_tabs = $this->option_fields();
    	foreach ($option_tabs as $option_tab) { //search all tabs
    		foreach ($option_tab['fields'] as $field) { //search all fields
    			if ($field['id'] == $field_id) {
    				return $option_tab['id'];
    			}
    		}
    	}
    	return $this->key; //return default key if field id not found
    }

	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed          Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		if ( 'option_metabox' === $field ) {
            return $this->option_fields();
        }
		throw new Exception( 'Invalid property: ' . $field );
	}

}


/**
 * Helper function to get/return the theme_admin object
 * @since  0.1.0
 * @return theme_admin object
 */
function theme_admin() {
	static $object = null;
	if ( is_null( $object ) ) {
		$object = new Theme_admin();
		$object->hooks();
	}

	return $object;
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function theme_get_option( $key = '' ) {
	return cmb2_get_option( theme_admin()->get_option_key($key), $key );
}
// Get it started
theme_admin();