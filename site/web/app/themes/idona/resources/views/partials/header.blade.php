@php($logo = theme_get_option('logo'))
@php($bio = theme_get_option('bio-protocol'))
<header class="banner">
  <div class="container">
    <a class="brand" href="{{ home_url('/') }}"><img src="{{$logo}}" class="img-responsive" alt="{{get_bloginfo('name', 'display')}}"/></a>
    <nav class="navbar navbar-toggleable-md">
      @if (has_nav_menu('primary_navigation'))
        <a class="logo-xs" href="{{ home_url('/') }}"><img src="{{$logo}}" class="img-responsive" alt="{{get_bloginfo('name', 'display')}}"/></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div id="navbar" class="navbar-collapse collapse ">
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']) !!}
          <div class="bio">
            <img src="{{$bio}}" class="img-responsive" alt="bio-procol"/>
          </div>
        </div>
      @endif
      
    </nav>
    
  </div>
</header>
