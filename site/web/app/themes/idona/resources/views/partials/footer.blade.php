
 @php
    $fb = theme_get_option('fb');
    $tw = theme_get_option('tw');
    $inst = theme_get_option('inst');
    $bio = theme_get_option('bio-protocol-alt');
    $logo = theme_get_option('alt_logo');
@endphp

<footer class="content-info container-fluid">
  <div class="social row">
    <div class="container">
      <ul>
        <li><a href="{{$fb}}" target="_blank"><i class="fa fa-facebook-square fa-2x"><span class="hidden-md-down"> vuélvete fan</span></i></a></li>
        <li><a href="{{$tw}}" target="_blank"><i class="fa fa-twitter-square fa-2x"><span class="hidden-md-down"> síguenos</span></i></a></li>
        <li><a href="{{$inst}}" target="_blank"><i class="fa fa-instagram fa-2x"><span class="hidden-md-down"> míranos</span></i></a></li>
      </ul>
    </div>
  </div>
  <div class="container">
     @if (has_nav_menu('footer'))
      {!! wp_nav_menu(['theme_location' => 'footer', 'menu_class' => 'nav']) !!}
     @endif
     <div class="logos">
       <a class="brand" href="{{ home_url('/') }}"><img src="{{$logo}}" class="img-responsive" alt="{{get_bloginfo('name', 'display')}}"/></a>
       <div class="bio">
        <img src="{{$bio}}" class="img-responsive" alt="bio-procol"/>
      </div>
     </div>
  </div>
</footer>
