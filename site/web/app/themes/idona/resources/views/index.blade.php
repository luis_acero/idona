@extends('layouts.app')
@php
      global $post;
      $slider = theme_get_option('home-slider');
      $titulo_1 = theme_get_option('title-section-1');
      $copy_1 = theme_get_option('copy-section-1');
      $mini_banner = theme_get_option('bottom-seccion-1');
      $green_block1 = theme_get_option('green-block1');
@endphp
@foreach($mini_banner as $item)
  @php
    $imagen = $item['image'];
    $title = $item['title'];
    $copy = $item['copy'];
  @endphp
@endforeach
  
@section('slider')
  
  <div class="container-fluid">
    <!-- slider -->
    <div class="row">
        <div id="slider" class="slider">
            {!!do_shortcode($slider)!!}
        </div>
    </div>
</div>
@endsection

@section('content')
  <section class="one">
      <h1>{!! $titulo_1 !!}</h1>
      <div class="col-xs-12 col-sm-8">
        <div class="row">
          {!!wpautop($copy_1)!!}
        </div>
      </div>
      <div class="col-xs-12">
        <figure>
          <img src="{!!$imagen!!}" alt="{!!$title!!}" class="img-responsive">
          <figcaption>
            <h2>{!!$title!!}</h2>
            {!!wpautop($copy)!!}
          </figcaption>
        </figure>
      </div>
  </section>
  <section class="green-block">
    <h2>{!!$green_block1!!}</h2>
  </section>
  <section class="productos">
    <h1>{{__('Productos','idn_theme')}}</h1>
    @php
        $args = array(
          'post_type' => 'product',
          'pust_status' => 'publish',
          'posts_per_page' => 1,
          'orderby' => 'modify',
            'meta_query' => array(
                array(
                    'key' => 'idn_producto-destacado',
                    'value' => 'on'
                ),
            )
        ); 
        $destacado = New WP_Query($args);
    @endphp
    @if(!$destacado->have_posts()) 
    <p>Lo sentimos no tenemos productos destacados en el momento</p>
    @endif
    @while($destacado->have_posts()) @php($destacado->the_post())
      @php
          $aux_img = get_post_meta($post->ID,__NAMESPACE__.'idn_imagen_plus',true);
      @endphp
      {!!the_title('<h2>','</h2>')!!}
      <div class="row">
        <div class="col-xs-12 col-sm-8 text-right">
            <h3>{!!the_excerpt()!!}</h3>
            <a href="{{the_permalink()}}" class="btn-type-01">comprar ahora</a>
            <div class="bottom">
            @if($aux_img)
                <img src="{!!$aux_img!!}" alt="">                
            @endif            
            {{the_content()}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
          {!!the_post_thumbnail('full', array('class' => 'img-responsive'))!!}
        </div>
      </div>

    @endwhile
    @php(wp_reset_postdata())
  </section>
  
@endsection
